Network traffic measurement
From Wikipedia, the free encyclopedia
Jump to navigationJump to search
In computer networks, network traffic measurement is the process of measuring the amount and type of traffic on a particular network. This is especially important with regard to effective bandwidth management.


Contents
1	Techniques
2	Measurement studies
3	Tools
3.1	Functions and features
4	See also
5	References
6	External links
Techniques
Network performance could be measured using either active or passive techniques. Active techniques (e.g. Iperf) are more intrusive but are arguably more accurate. Passive techniques have less network overhead and hence can run in the background to be used to trigger network management actions.

Measurement studies
A range of studies have been performed from various points on the Internet. The AMS-IX (Amsterdam Internet Exchange) is one of the world's largest Internet exchanges. It produces a constant supply of simple Internet statistics. There are also numerous academic studies that have produced a range of measurement studies[1][2][3] on frame size distributions, TCP/UDP ratios and TCP/IP options.

Tools
Various software tools are available to measure network traffic. Some tools measure traffic by sniffing and others use SNMP, WMI or other local agents to measure bandwidth use on individual machines and routers. However, the latter generally do not detect the type of traffic, nor do they work for machines which are not running the necessary agent software, such as rogue machines on the network, or machines for which no compatible agent is available. In the latter case, inline appliances are preferred. These would generally 'sit' between the LAN and the LAN's exit point, generally the WAN or Internet router, and all packets leaving and entering the network would go through them. In most cases the appliance would operate as a bridge on the network so that it is undetectable by users.

Some tools used for SNMP monitoring are InfoVista[4], Tivoli Netcool/Proviso [5] by IBM, CA Performance Management by CA Technologies[6], TotalView [7] by PathSolutions, SolarWinds[8], and NMIS from Opmantek.

Functions and features
Measurement tools generally have these functions and features:

User interface (web, graphical, console)
Real-time traffic graphs
Network activity is often reported against pre-configured traffic matching rules to show:
Local IP address
Remote IP address
Port number or protocol
Logged in user name
Bandwidth quotas
Support for traffic shaping or rate limiting (overlapping with the network traffic control page)
Support website blocking and content filtering
Alarms to notify the administrator of excessive usage (by IP address or in total)
See also
IP Flow Information Export and NetFlow
Measuring network throughput
Network management
Network monitoring
Network scheduler
Network simulation
Packet sniffer
Performance management
References
 Murray, David; Terry Koziniec (2012). "The State of Enterprise Network Traffic in 2012". 18th Asia-Pacific Conference on Communications (APCC 2012).
 Zhang, Min; Maurizio Dusi; Wolfgang John; Changjia Chen (2009). "Analysis of udp traffic usage on internet backbone links". In Proceedings of the 2009 Ninth Annual International Symposium on Applications and the Internet.
 Wolfgang, John; Sven Tafvelin (2007). "Analysis of internet backbone traffic and header anomalies observed". ACM Wireless Networks. Proceedings of the 7th ACM SIGCOMM conference on Internet measurement.
 "Product Overview". InfoVista.com. Retrieved 27 September 2018.
 "Configuring IBM Tivoli Storage Manager SNMP". ibm.com. Retrieved 27 September 2018.
 "CA Performance Management - 2.8". docops.ca.com. Retrieved 27 September 2018.
 "Selecting The Right Network Troubleshooting Tool Part Three: SNMP". PathSolutions.com. Retrieved 27 September 2018.
 "SNMP Monitoring". SolarWinds.com. Retrieved 27 September 2018.

