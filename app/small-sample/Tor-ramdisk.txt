


Tor-ramdisk - Wikipedia
document.documentElement.className="client-js";RLCONF={"wgBreakFrames":!1,"wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRequestId":"Xhcu4gpAMFYAAFjgMfoAAABH","wgCSPNonce":!1,"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":!1,"wgNamespaceNumber":0,"wgPageName":"Tor-ramdisk","wgTitle":"Tor-ramdisk","wgCurRevisionId":918702418,"wgRevisionId":918702418,"wgArticleId":21180933,"wgIsArticle":!0,"wgIsRedirect":!1,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["Official website different in Wikidata and Wikipedia","All stub articles","Light-weight Linux distributions","Tor (anonymity network)","Tor (anonymity network) stubs"],"wgPageContentLanguage":"en",
"wgPageContentModel":"wikitext","wgRelevantPageName":"Tor-ramdisk","wgRelevantArticleId":21180933,"wgIsProbablyEditable":!0,"wgRelevantPageIsProbablyEditable":!0,"wgRestrictionEdit":[],"wgRestrictionMove":[],"wgMediaViewerOnClick":!0,"wgMediaViewerEnabledByDefault":!0,"wgPopupsReferencePreviews":!1,"wgPopupsConflictsWithNavPopupGadget":!1,"wgVisualEditor":{"pageLanguageCode":"en","pageLanguageDir":"ltr","pageVariantFallbacks":"en"},"wgMFDisplayWikibaseDescriptions":{"search":!0,"nearby":!0,"watchlist":!0,"tagline":!1},"wgWMESchemaEditAttemptStepOversample":!1,"wgULSCurrentAutonym":"English","wgNoticeProject":"wikipedia","wgWikibaseItemId":"Q7825228","wgCentralAuthMobileDomain":!1,"wgEditSubmitButtonLabelPublish":!0};RLSTATE={"ext.globalCssJs.user.styles":"ready","site.styles":"ready","noscript":"ready","user.styles":"ready","ext.globalCssJs.user":"ready","user":"ready","user.options":"ready","user.tokens":"loading","ext.cite.styles":"ready",
"mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","jquery.makeCollapsible.styles":"ready","wikibase.client.init":"ready","ext.visualEditor.desktopArticleTarget.noscript":"ready","ext.uls.interlanguage":"ready","ext.wikimediaBadges":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"};RLPAGEMODULES=["ext.cite.ux-enhancements","site","mediawiki.page.startup","mediawiki.page.ready","jquery.makeCollapsible","mediawiki.searchSuggest","ext.gadget.ReferenceTooltips","ext.gadget.watchlist-notice","ext.gadget.DRN-wizard","ext.gadget.charinsert","ext.gadget.refToolbar","ext.gadget.extra-toolbar-buttons","ext.gadget.switcher","ext.centralauth.centralautologin","mmv.head","mmv.bootstrap.autostart","ext.popups","ext.visualEditor.desktopArticleTarget.init","ext.visualEditor.targetLoader","ext.eventLogging","ext.wikimediaEvents","ext.navigationTiming","ext.uls.compactlinks","ext.uls.interface","ext.cx.eventlogging.campaigns","ext.quicksurveys.init",
"ext.centralNotice.geoIP","ext.centralNotice.startUp","skins.vector.js"];
(RLQ=window.RLQ||[]).push(function(){mw.loader.implement("user.tokens@tffin",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});});





























Tor-ramdisk

From Wikipedia, the free encyclopedia


Jump to navigation
Jump to search
Tor-ramdiskDeveloperAnthony G. BasileOS familyUnix-likeWorking stateCurrentSource modelFree softwareLatest release20170130 / January 30, 2017; 2 years ago (2017-01-30)[1]Repositorygitweb.torproject.org/tor-ramdisk.git
 Available inEnglishPlatformsi686, MIPS,[2] x86-64[3]Kernel typeMonolithic (Linux)Default user interfaceTextLicenseGPL v3Official websitegitweb.torproject.org/tor-ramdisk.git
Tor-ramdisk is an i686 uClibc-based micro Linux distribution whose only purpose is to host a Tor server in an environment that maximizes security and privacy. Security is enhanced in Tor-ramdisk by employing a monolithically compiled GRSEC/PaX patched kernel and hardened system tools. Privacy is enhanced by turning off logging at all levels so that even the Tor operator only has access to minimal information. Finally, since everything runs in ephemeral memory, no information survives a reboot, except for the Tor configuration file and the private RSA key, which may be exported and imported by FTP or SCP.

See also[edit]


Free and open-source software portal
Linux portal

Crypto-anarchism
Cypherpunk
Free Haven Project
Hacktivism
Internet censorship
Internet privacy
Ramdisk
Real-time adaptive security
Security-Enhanced Linux

References[edit]


^ "Tor-ramdisk official i686 download". Retrieved 2018-07-24..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}

^ "Tor-ramdisk MIPS Port". Archived from the original on 2016-04-28. Retrieved 2011-04-15.

^ "Tor-ramdisk x86_64 Port". Retrieved 2018-07-27.


External links[edit]
Official website
vteTorPeople
Caspar Bowden
Roger Dingledine
Ian Goldberg
Wendy Seltzer
Technologies
Tor
.onion
onion routing
Software
Vidalia
Orbot
Ricochet
Tails
Tor-ramdisk
Tor2web
TorChat
Related topics
The Tor Project
HTTPS Everywhere
List of Tor onion services
Operation Onymous
Operation Bayonet
Darknet
Dark web
I2P
Freenet
GNUnet
Online anonymity

 Category
 Commons

vteLinux distributionsAndroid
Android-IA
Android-x86
GrapheneOS
LineageOS
/e/
CyanogenMod
MIUI
Remix OS
Replicant
Resurrection Remix OS
OmniROM
Arch
ArchBang
ArchLabs
Artix
Chakra
Hyperbola GNU/Linux-libre
Manjaro
Parabola GNU/Linux-libre
Debian
antiX
Astra Linux
Bharat Operating System Solutions
deepin
Devuan
Endless OS
gNewSense
HandyLinux
Kali Linux
Knoppix
MX Linux
Parrot Security OS
SparkyLinux
SolydXK
SteamOS
Tails
Ubuntu
Official: Kubuntu
Lubuntu
Ubuntu Budgie
Ubuntu Kylin
Ubuntu MATE
Ubuntu Studio
Xubuntu
Other: Asturix
Bodhi Linux
elementary OS
KDE neon
Linux Mint
Pinguy OS
Trisquel
Uruk
LXLE Linux
Linux Lite
Zorin OS

Fedora
BLAG Linux and GNU
Korora
Red Hat
CentOS
ClearOS
Linpus Linux
Oracle Linux
Qubes OS
Rocks Cluster Distribution
Scientific Linux
SME Server

Gentoo
Calculate Linux
Chromium OS
Chrome OS
Sabayon Linux
Nova OS
Slackware
Austrumi Linux
DeLi Linux
DNALinux
Kongoni
NimbleX
Platypux
Porteus
Salix OS
Slax
TopologiLinux
VectorLinux
Zenwalk
Mandriva
ALT Linux
Mageia
OpenMandriva Lx
PCLinuxOS
ROSA Linux
Other
4MLinux
Alpine Linux
CRUX
Frugalware Linux
GeckoLinux
GoboLinux
GuixSD
KaOS
Kwort Linux
Lunar Linux
NixOS
openSUSE
SUSE Linux Enterprise Server
Puppy Linux
Source Mage
Solus
Void Linux
Uruk GNU/Linux

 Category
 Comparison
 List
 Commons



This article related to the Tor network is a stub. You can help Wikipedia by expanding it.vte




Retrieved from "https://en.wikipedia.org/w/index.php?title=Tor-ramdisk&oldid=918702418"
Categories: Light-weight Linux distributionsTor (anonymity network)Tor (anonymity network) stubsHidden categories: Official website different in Wikidata and WikipediaAll stub articles







Navigation menu


Personal tools

Not logged inTalkContributionsCreate accountLog in 



Namespaces

ArticleTalk





Variants







Views

ReadEditView history




More





Search















Navigation


Main pageContentsFeatured contentCurrent eventsRandom articleDonate to WikipediaWikipedia store 



Interaction


HelpAbout WikipediaCommunity portalRecent changesContact page 



Tools


What links hereRelated changesUpload fileSpecial pagesPermanent linkPage informationWikidata itemCite this page 



Print/export


Create a bookDownload as PDFPrintable version 



Languages


FrançaisItalianoNorsk bokmål 
Edit links 





 This page was last edited on 29 September 2019, at 23:13 (UTC).
Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization.


Privacy policy
About Wikipedia
Disclaimers
Contact Wikipedia
Developers
Statistics
Cookie statement
Mobile view



 

 



(RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.484","walltime":"0.674","ppvisitednodes":{"value":1181,"limit":1000000},"ppgeneratednodes":{"value":0,"limit":1500000},"postexpandincludesize":{"value":55289,"limit":2097152},"templateargumentsize":{"value":2045,"limit":2097152},"expansiondepth":{"value":22,"limit":40},"expensivefunctioncount":{"value":3,"limit":500},"unstrip-depth":{"value":1,"limit":20},"unstrip-size":{"value":8315,"limit":5000000},"entityaccesscount":{"value":1,"limit":400},"timingprofile":["100.00%  539.764      1 -total"," 47.76%  257.785      1 Template:Infobox_OS"," 43.03%  232.255      1 Template:Infobox"," 26.93%  145.376      1 Template:Reflist"," 24.22%  130.704      3 Template:Cite_web"," 14.20%   76.663      2 Template:Wikidata"," 10.33%   55.750      1 Template:Start_date_and_age"," 10.27%   55.417      4 Template:Navbox","  6.57%   35.451      1 Template:Tor_project","  5.80%   31.309      1 Template:Time_ago"]},"scribunto":{"limitreport-timeusage":{"value":"0.214","limit":"10.000"},"limitreport-memusage":{"value":4838492,"limit":52428800}},"cachereport":{"origin":"mw1245","timestamp":"20200107213950","ttl":2592000,"transientcontent":false}}});});
{"@context":"https:\/\/schema.org","@type":"Article","name":"Tor-ramdisk","url":"https:\/\/en.wikipedia.org\/wiki\/Tor-ramdisk","sameAs":"http:\/\/www.wikidata.org\/entity\/Q7825228","mainEntity":"http:\/\/www.wikidata.org\/entity\/Q7825228","author":{"@type":"Organization","name":"Contributors to Wikimedia projects"},"publisher":{"@type":"Organization","name":"Wikimedia Foundation, Inc.","logo":{"@type":"ImageObject","url":"https:\/\/www.wikimedia.org\/static\/images\/wmf-hor-googpub.png"}},"datePublished":"2009-01-20T18:36:07Z","dateModified":"2019-09-29T23:13:22Z","headline":"a uClibc-based minimalist distribution of GNU\/Linux whose only purpose is to host a Tor server in an environment that maximizes security and privacy"}
(RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgBackendResponseTime":118,"wgHostname":"mw1251"});});


