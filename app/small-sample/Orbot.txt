


Orbot - Wikipedia
document.documentElement.className="client-js";RLCONF={"wgBreakFrames":!1,"wgSeparatorTransformTable":["",""],"wgDigitTransformTable":["",""],"wgDefaultDateFormat":"dmy","wgMonthNames":["","January","February","March","April","May","June","July","August","September","October","November","December"],"wgMonthNamesShort":["","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"],"wgRequestId":"XhUNMApAAEUAAE4vCjEAAAAY","wgCSPNonce":!1,"wgCanonicalNamespace":"","wgCanonicalSpecialPageName":!1,"wgNamespaceNumber":0,"wgPageName":"Orbot","wgTitle":"Orbot","wgCurRevisionId":917336412,"wgRevisionId":917336412,"wgArticleId":34650947,"wgIsArticle":!0,"wgIsRedirect":!1,"wgAction":"view","wgUserName":null,"wgUserGroups":["*"],"wgCategories":["Use dmy dates from July 2012","Pages using deprecated image syntax","All stub articles","Free and open-source Android software","Free software programmed in Java (programming language)","Tor (anonymity network)",
"Tor (anonymity network) stubs"],"wgPageContentLanguage":"en","wgPageContentModel":"wikitext","wgRelevantPageName":"Orbot","wgRelevantArticleId":34650947,"wgIsProbablyEditable":!0,"wgRelevantPageIsProbablyEditable":!0,"wgRestrictionEdit":[],"wgRestrictionMove":[],"wgMediaViewerOnClick":!0,"wgMediaViewerEnabledByDefault":!0,"wgPopupsReferencePreviews":!1,"wgPopupsConflictsWithNavPopupGadget":!1,"wgVisualEditor":{"pageLanguageCode":"en","pageLanguageDir":"ltr","pageVariantFallbacks":"en"},"wgMFDisplayWikibaseDescriptions":{"search":!0,"nearby":!0,"watchlist":!0,"tagline":!1},"wgWMESchemaEditAttemptStepOversample":!1,"wgULSCurrentAutonym":"English","wgNoticeProject":"wikipedia","wgWikibaseItemId":"Q16927751","wgCentralAuthMobileDomain":!1,"wgEditSubmitButtonLabelPublish":!0};RLSTATE={"ext.globalCssJs.user.styles":"ready","site.styles":"ready","noscript":"ready","user.styles":"ready","ext.globalCssJs.user":"ready","user":"ready","user.options":"ready",
"user.tokens":"loading","ext.cite.styles":"ready","mediawiki.legacy.shared":"ready","mediawiki.legacy.commonPrint":"ready","jquery.makeCollapsible.styles":"ready","wikibase.client.init":"ready","ext.visualEditor.desktopArticleTarget.noscript":"ready","ext.uls.interlanguage":"ready","ext.wikimediaBadges":"ready","mediawiki.skinning.interface":"ready","skins.vector.styles":"ready"};RLPAGEMODULES=["ext.cite.ux-enhancements","site","mediawiki.page.startup","mediawiki.page.ready","jquery.makeCollapsible","mediawiki.searchSuggest","ext.gadget.ReferenceTooltips","ext.gadget.watchlist-notice","ext.gadget.DRN-wizard","ext.gadget.charinsert","ext.gadget.refToolbar","ext.gadget.extra-toolbar-buttons","ext.gadget.switcher","ext.centralauth.centralautologin","mmv.head","mmv.bootstrap.autostart","ext.popups","ext.visualEditor.desktopArticleTarget.init","ext.visualEditor.targetLoader","ext.eventLogging","ext.wikimediaEvents","ext.navigationTiming","ext.uls.compactlinks","ext.uls.interface",
"ext.cx.eventlogging.campaigns","ext.quicksurveys.init","ext.centralNotice.geoIP","ext.centralNotice.startUp","skins.vector.js"];
(RLQ=window.RLQ||[]).push(function(){mw.loader.implement("user.tokens@tffin",function($,jQuery,require,module){/*@nomin*/mw.user.tokens.set({"patrolToken":"+\\","watchToken":"+\\","csrfToken":"+\\"});
});});






























Orbot

From Wikipedia, the free encyclopedia


Jump to navigation
Jump to search
This article is about the software. For the Sonic character, see Orbot and Cubot. For series, see The Mighty Orbots.


OrbotDeveloper(s)The Tor ProjectInitial release1 October 2008 (2008-10-01)Stable release16.0.0-RC-2
   / 5 January 2018 (2018-01-05)Preview release15.2.0 RC 2 (29 October 2016; 3 years ago (2016-10-29)[1]) [±]
Repositorygitweb.torproject.org/orbot.git
 Written inJavaOperating systemAndroid[2]Size12.63 MBLicense3-clause BSD licenseWebsitewww.guardianproject.info/apps/orbot
Orbot is a free software Proxy server project to provide anonymity on the Internet for users of the Android operating system. It acts as an instance of the Tor network on such devices and allows traffic routing from a device's web browser, e-mail client, map program, etc., through the Tor network, providing anonymity for the user.[2]
This tool is used to keep the communications of users anonymized and hidden from governments and third parties that might be monitoring their internet traffic.[3]

References[edit]


^ The Tor Project (29 October 2016). "Orbot: Proxy with Tor". Google Play. Google. Retrieved 30 October 2016..mw-parser-output cite.citation{font-style:inherit}.mw-parser-output .citation q{quotes:"\"""\"""'""'"}.mw-parser-output .citation .cs1-lock-free a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/6/65/Lock-green.svg/9px-Lock-green.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-limited a,.mw-parser-output .citation .cs1-lock-registration a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/d/d6/Lock-gray-alt-2.svg/9px-Lock-gray-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .citation .cs1-lock-subscription a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/a/aa/Lock-red-alt-2.svg/9px-Lock-red-alt-2.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration{color:#555}.mw-parser-output .cs1-subscription span,.mw-parser-output .cs1-registration span{border-bottom:1px dotted;cursor:help}.mw-parser-output .cs1-ws-icon a{background:url("//upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/12px-Wikisource-logo.svg.png")no-repeat;background-position:right .1em center}.mw-parser-output code.cs1-code{color:inherit;background:inherit;border:inherit;padding:inherit}.mw-parser-output .cs1-hidden-error{display:none;font-size:100%}.mw-parser-output .cs1-visible-error{font-size:100%}.mw-parser-output .cs1-maint{display:none;color:#33aa33;margin-left:0.3em}.mw-parser-output .cs1-subscription,.mw-parser-output .cs1-registration,.mw-parser-output .cs1-format{font-size:95%}.mw-parser-output .cs1-kern-left,.mw-parser-output .cs1-kern-wl-left{padding-left:0.2em}.mw-parser-output .cs1-kern-right,.mw-parser-output .cs1-kern-wl-right{padding-right:0.2em}

^ a b Shakeel, Irfan (April 2011). "Orbot: Tor- Anonymous On Android". eHacking.net. Retrieved 21 July 2012.

^ Farivar, Cyrus (15 February 2012). "From encryption to darknets: As governments snoop, activists fight back". Ars Technica. Retrieved 21 July 2012.


Further reading[edit]
Hathaway, Jay (19 April 2010). "Orbot offers anonymous browsing on Android, via Tor". Switched.com. Archived from the original on 3 February 2013. Retrieved 21 July 2012.


vteTorPeople
Caspar Bowden
Roger Dingledine
Ian Goldberg
Wendy Seltzer
Technologies
Tor
.onion
onion routing
Software
Vidalia
Orbot
Ricochet
Tails
Tor-ramdisk
Tor2web
TorChat
Related topics
The Tor Project
HTTPS Everywhere
List of Tor onion services
Operation Onymous
Operation Bayonet
Darknet
Dark web
I2P
Freenet
GNUnet
Online anonymity

 Category
 Commons

This article related to the Tor network is a stub. You can help Wikipedia by expanding it.vte




Retrieved from "https://en.wikipedia.org/w/index.php?title=Orbot&oldid=917336412"
Categories: Free and open-source Android softwareFree software programmed in Java (programming language)Tor (anonymity network)Tor (anonymity network) stubsHidden categories: Use dmy dates from July 2012Pages using deprecated image syntaxAll stub articles







Navigation menu


Personal tools

Not logged inTalkContributionsCreate accountLog in 



Namespaces

ArticleTalk





Variants







Views

ReadEditView history




More





Search















Navigation


Main pageContentsFeatured contentCurrent eventsRandom articleDonate to WikipediaWikipedia store 



Interaction


HelpAbout WikipediaCommunity portalRecent changesContact page 



Tools


What links hereRelated changesUpload fileSpecial pagesPermanent linkPage informationWikidata itemCite this page 



In other projects


Wikimedia Commons 



Print/export


Create a bookDownload as PDFPrintable version 



Languages


العربيةবাংলাEspañolEuskaraNederlands日本語中文 
Edit links 





 This page was last edited on 23 September 2019, at 11:08 (UTC).
Text is available under the Creative Commons Attribution-ShareAlike License;
additional terms may apply.  By using this site, you agree to the Terms of Use and Privacy Policy. Wikipedia® is a registered trademark of the Wikimedia Foundation, Inc., a non-profit organization.


Privacy policy
About Wikipedia
Disclaimers
Contact Wikipedia
Developers
Statistics
Cookie statement
Mobile view



 

 



(RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgPageParseReport":{"limitreport":{"cputime":"0.384","walltime":"0.596","ppvisitednodes":{"value":1462,"limit":1000000},"ppgeneratednodes":{"value":0,"limit":1500000},"postexpandincludesize":{"value":39341,"limit":2097152},"templateargumentsize":{"value":2652,"limit":2097152},"expansiondepth":{"value":30,"limit":40},"expensivefunctioncount":{"value":3,"limit":500},"unstrip-depth":{"value":1,"limit":20},"unstrip-size":{"value":10990,"limit":5000000},"entityaccesscount":{"value":1,"limit":400},"timingprofile":["100.00%  529.227      1 -total"," 57.34%  303.445      2 Template:Infobox"," 43.98%  232.760      1 Template:Infobox_software"," 24.80%  131.248      1 Template:Reflist"," 23.98%  126.919      4 Template:Cite_web"," 16.30%   86.243      1 Template:Infobox_software/simple"," 12.55%   66.400      1 Template:About"," 11.17%   59.114      1 Template:Latest_preview_software_release/Orbot","  9.38%   49.628      2 Template:Wikidata","  6.79%   35.951      1 Template:Use_dmy_dates"]},"scribunto":{"limitreport-timeusage":{"value":"0.183","limit":"10.000"},"limitreport-memusage":{"value":4521597,"limit":52428800}},"cachereport":{"origin":"mw1274","timestamp":"20200107225857","ttl":2592000,"transientcontent":false}}});});
{"@context":"https:\/\/schema.org","@type":"Article","name":"Orbot","url":"https:\/\/en.wikipedia.org\/wiki\/Orbot","sameAs":"http:\/\/www.wikidata.org\/entity\/Q16927751","mainEntity":"http:\/\/www.wikidata.org\/entity\/Q16927751","author":{"@type":"Organization","name":"Contributors to Wikimedia projects"},"publisher":{"@type":"Organization","name":"Wikimedia Foundation, Inc.","logo":{"@type":"ImageObject","url":"https:\/\/www.wikimedia.org\/static\/images\/wmf-hor-googpub.png"}},"datePublished":"2012-02-08T12:09:10Z","dateModified":"2019-09-23T11:08:53Z","image":"https:\/\/upload.wikimedia.org\/wikipedia\/commons\/8\/8b\/Orbot-logo.svg","headline":"free software project to provide anonymity on the Internet from a Google Android smartphone."}
(RLQ=window.RLQ||[]).push(function(){mw.config.set({"wgBackendResponseTime":709,"wgHostname":"mw1274"});});


