# SEARCH ENGINE SCRIPT, WITH MULTIPLE URLS LEADING TO DIFFERENT PAGES TO SEARCH OR INDEX PAGES
import tiny
from flask import Flask, render_template, request
import mysql.connector
import scraper

no_ads_string = "Sick of your husband? Join us for regular meetings to talk. Click here!"


#  CONNECT TO DB
search = mysql.connector.connect(
   host="mysql-read",
   user="root",
   passwd="",
   database="search"
)

mycursor = search.cursor()

my_index = tiny.Index("app/small-sample")
app = Flask(__name__)

# RENDER HOMEPAGE OF SEARCH
@app.route("/")
def root():
    return render_template("index.html")


# SEARCH METHOD
@app.route("/search")
def search():
    q = request.args['q']
    results = my_index.search(q)
    keyword = str(q)
    # GET ADS ASSOCIATED WITH SEARCH
    sql = "SELECT advert FROM advert WHERE keyword = '" + keyword + "'"
    mycursor.execute(sql)
    listitems = mycursor.fetchall()
    # PUT FILLER INTO ADS LIST SO AS IT WILL RETURN SOMETHING TO THE HTML
    list_items = []
    if len(list_items) == 0:
        list_items.append(no_ads_string)

    # RETURN RESULTS WITH ADS
    return render_template("results.html", q=q, results=results, list_items=list_items)

# SCRAPE HOMEPAGE
@app.route("/scrape")
def scrape():
    return render_template("scrape.html")

# SCRAPE METHOD
@app.route("/scrapePage")
def scrape_page():
    q = request.args['q']
    print(q)
    scraper.scrapeURL(q)
    tiny.make_index("app/small-sample")
    return render_template('scrapeResults.html')

# RUN MAIN
if __name__ == '__main__':
	app.run(debug=False, host = '0.0.0.0', port = 5000)
