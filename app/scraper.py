from bs4 import BeautifulSoup
from urllib.request import urlopen

import random
import datetime
import sched, time
import mysql.connector


links_file = []

# THIS METHOD WILL BE CALLED WHEN A USER ENTERS A URL ON THE FRONT-END
def scrapeURL(url):
    html = urlopen(url).read().decode('utf-8')

    soup = BeautifulSoup(html, features='lxml')
    print(soup.h1)

    all_href = soup.find_all('a')
    scrape_links(all_href)
    input_info()



# ADDITIONAL FEATURES

# SCRAPE SITE X IN ORDER TO GET THE URLS TO SCRAPE FROM ITS HOMEPAGE ON WIKI
def scrape_links(all_href):
    for link in all_href:
        url = link.get('href')

        if url != None and url.startswith('/wiki'):
            links_file.append('https://en.wikipedia.org'+url)

# SCRAPE RANDOM WEBSITE FROM LIST OF URLS & INPUT INTO DB
def input_info():
    page_url = random.choice(links_file)
    html = urlopen(page_url).read().decode('utf-8')
    soupmix = BeautifulSoup(html, features='lxml')
    # print(soupmix.find_all('a'))
    title = soupmix.select('h1')[0].text.strip()
    print(title)

    # WRITE TO TEXT FILE FOR INDEXING
    f = open("app/small-sample/"+ title+'.txt',"w+", encoding="utf-8")
    f.write(soupmix.text)
    f.close()

    #  CONNECT TO DB
    search = mysql.connector.connect(
       host="mysql-0.mysql",
       user="root",
       passwd="",
       database="search"
    )

    mycursor = search.cursor()

    # INPUT INFO INTO THE SQL DB
    try:
        sql = "INSERT INTO page (uri, indexedat, content) VALUES (%s, %s, %s)"
        val = (page_url, datetime.datetime.now(), soupmix.text)
        mycursor.execute(sql, val)

        search.commit()
    except:
        print("Issue with inserting. Try another link.")
        input_info()
