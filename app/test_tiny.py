#   THIS SCRIPT TESTS A FUNCTION IN THE tiny.py FILE
#   IF IT PASSES IT SHOULD ENABLE GITLAB CI/CD PIPELINE
import tiny

import unittest

class Tiny(unittest.TestCase):

    def test_upper(self):
        self.assertEqual('foo'.upper(), 'FOO')


if __name__ == '__main__':
    unittest.main()
